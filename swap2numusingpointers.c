#include<stdio.h>
void swap(int *a,int *b);
int main()
{
int a,b;
printf("enter a & b\n");
scanf("%d%d",&a,&b);
printf("before swapping a=%d and b=%d\n",a,b);
swap(&a,&b);
printf("swapped numbers area a=%d b=%d\n",a,b);
return 0;
}
void swap(int *x,int *y)
{
int t;
t=*x;
*x=*y;
*y=t;
}