#include <stdio.h>

int input()
{
    char ch;
    printf("Enter your character:");
    scanf("%c",&ch);
    return ch;
}

void output(char ch)
{
    printf("The ASC|| value of %c is %d.",ch,ch);
}

int main()
{
    char ch =input ();
    output (ch);
    return 0;
}